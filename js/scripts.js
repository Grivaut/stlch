$(document).ready(function(){
	$('.cityset .link-hiddenlist').click(function(){
		/*$(this).parent().find('ul').toggleClass('openheaderlist');*/
		$('.cityset__list').toggleClass('openheaderlist');
	});
	
	$(document).mouseup(function (e){
		var menu = $('.cityset');
		if (!menu.is(e.target) && menu.has(e.target).length === 0) {
			$('.cityset .cityset__list').removeClass('openheaderlist');
		}
	});

/******************************************************/
	$('.catchange__set.link-hiddenlist').click(function(){
		$('.catchange .catchange__list').toggleClass('openheaderlist');
		$(this).toggleClass('catchange-open');
	});

	$(document).mouseup(function (e){
		var opencat = $('.catchange');
		if (!opencat.is(e.target) && opencat.has(e.target).length === 0) {
			$('.catchange-open').removeClass('catchange-open');
			$('.catchange .catchange__list').removeClass('openheaderlist');
		}
	});

/*****************************************************/
	$('.navigation__mob-btn .link-hiddenlist').click(function(){
		$(this).parent().siblings('nav.menu').toggleClass('openheaderlist');
		$(this).toggleClass('openmenu');
	});
	$(document).mouseup(function (e){
		var openmenu = $('.navigation__mob-btn .openmenu');
		if (!openmenu.is(e.target) && openmenu.has(e.target).length === 0) {
			openmenu.removeClass('openmenu');
			$('.menu').removeClass('openheaderlist');

		}
	});


	$(function(){
		if ( $(window).width() > 576 ) {
			$('ul.catalog-menu').flexMenu({
				showOnHover: false,
				linkText: "<span class='catalog-menu__burger'><span></span><span></span><span></span></span>",
				linkTitle: "Показать еще",
				linkTextAll: "<span class='d-flex align-items-center'><span class='catalog-menu__burger'><span></span><span></span><span></span></span>Смотреть все разделы</span>",
				linkTitleAll: "Все разделы",
				cutoff: 1,
				popupClass: 'more-dropdown'
			})
		}
	  });


});